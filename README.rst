FaQCs GeneFlow App
==================

Version: 2.10-01

This GeneFlow app wraps the FaQCs biocontainer.

Inputs
------

1. input: Input fastq file.

2. pair: Fastq file pair.

Parameters
----------

1. output: Output QC folder. Default: output.

